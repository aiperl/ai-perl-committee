# AI Perl Committee
# Meeting Agenda & Minutes

## February 8th, 2024
## Fourteenth Official Meeting

* Opening, 2:19pm

* Attendance & Introductions (already done in preceding meeting)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Zakariyya Mughal (BioPerl, PDL, previously biomedical imaging, currently computational toxicology)
    * Cheok Yin "CY" Fung (physics major, interested in math and quantum computing)

* Announcements
    * Chairman John Napiorkowski requests 1st Vice Chairman Will Braswell to preside
    * Meet every 2 weeks on Thursday 2:00-3:00pm Central time zone
        * Immediately following Science Perl Committee and Perl::Types Committee meetings

* Previous Meeting's Minutes
    * 1/25/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Zaki Mughal to be accepted as read, voted unanimously

* Old Business

    * TensorFlow
        * On hold until other Zaki project is complete

        * RoboPerl nonprofit grant, approved for these 3 TensorFlow projects:
            * Object Detection
                * Need to work w/ Zaki to get Docker integrated into Navi AI
            * Speech Recognition
                * Started with Zaki at last AI Perl Hackathon, see 11/30/23 meeting notes for details & next steps
                    * https://gitlab.com/aiperl/ai-perl-committee/-/blob/main/meetings/20231130_meeting_agenda_and_minutes.md
                * Need to continue working on this with Zaki, possibly find someone to help if they exist?
            * Optical Character Recognition
                * Not started yet

    * PerlGPT
        * Phase 1, TPF grant re-proposal, approved
            * Need to write initial report and send to TPF ASAP
        * Repository
            * git@gitlab.com:aiperl/perlgpt.git
        * We are using the MetaCPAN API directly
            * Search through all CPAN for good training distributions
            * Must be recent, last 3 - 5 years to start
            * Must pass CPAN Testers with high pass rates, 90% or higher
            * Must pass CPANTS Kwalitee with high score, 25 out of 30 or higher
            * Automatically annotate Perl::Critic violations with high severity, 4 out of 5 or higher
            * Organize into granularity of less than 4K tokens per collection of code + docs
                * Distribution
                * Files
                * Packages / Classes
                * Subroutines
            * Somehow give preference to well-written code containing descriptive comments?
            * Current issues
                * UTF8
                    * Perl::Critic crashes on several CPAN distributions with files containing special characters
                        * Can't parse code: Encountered unexpected character '226'
                        * https://fastapi.metacpan.org/source/PEVANS/Syntax-Operator-In-0.09/t/02elem-numbery.t
                    * Existing PPI and/or Perl::Critic issue(s)
                        * https://github.com/Perl-Critic/PPI/issues/22
                        * https://github.com/Perl-Critic/Perl-Critic/issues/562
                * Scroll API
                    * Perl code showing the scroll API working
                        * https://github.com/metacpan/metacpan-examples/blob/main/scripts/author/1b-scroll-all-authors-es.pl
                    * Ruby code showing the scroll API working
                        * https://github.com/librariesio/libraries.io/pull/2817/files
                * Memory leak
                    * Leaking memory, probably from Perl::Critic's PPI having lots of self-references that never get weakened and thus never get freed
                    * Need Parallel::ForkManager, instead of trying to find somebody else's PPI memory leak
                        * Fork new process for each new distribution loop iteration
                        * Killing each child process will release that Perl interpreter's memory heap
                        * Should limit memory leak to one distribution at a time, which is fine and will not grow memory to lock up OS
                    * Several memory leak solutions exist, only tried the first 3 so far and they did not give immediately helpful info
                        * https://metacpan.org/pod/Devel::LeakTrace
                        * https://metacpan.org/pod/Test::LeakTrace
                        * https://metacpan.org/pod/Devel::Leak::Object
                        * https://metacpan.org/pod/Devel::Size
                        * https://metacpan.org/pod/Devel::SizeMe
                            * https://blog.timbunce.org/2012/10/05/introducing-develsizeme-visualizing-perl-memory-use/
                        * https://metacpan.org/pod/Test::Valgrind
                        * https://metacpan.org/pod/Devel::MAT
                            * https://metacpan.org/dist/Devel-MAT/view/lib/Devel/MAT/UserGuide.pod

    * AI Perl Hackathon
        * Every 6 weeks
        * Next event 2/21/24, 7:00-9:00pm Central time zone, theme is TensorFlow Speech Recognition

    * Navi AI & robot
        * John & Will got initial refactoring Camel (Catalyst MVC) running, need refactor webcam code w/ Zaki
        * Need Robert Grimes to help get Navi Dockers running locally for John, so he can continue Camel upgrades
        * Navi single-arm robot prototype currently being tested at Diogenes Hackerspace
 
    * Cowl API
        * Currently under development, component of Navi's world map
        * Next steps
            * Clean up API
            * Finish 2 remaining tests for streaming reading & writing ontologies
            * Align Cowl & Perl's independent reference counting
            * Write documentation
            * CPAN release
            * Docker
            * Navi integration

    * OpenAI::API
        * Need completely-compatible OpenAI client, collaborating with Ryan and John
        * Ryan Voots is developing OpenAI-compatible API using low-level Perl libraries on top of Python LLM loading/inference
        * Ryan is working on SSE already, we will let him show us his SSE and tell him if we like it
        * When Ryan has something ready to test, John can test it and get back with results

    * LangChain
        * John wants to start porting LangChain to Perl
        * We will use our prototype Python-to-Perl translator to get started, then invite others to join the project
        * Repositories
            * https://gitlab.com/aiperl/langchain
            * https://gitlab.com/aiperl/langchain-perl

    * Ollama
        * From AI Perl Committee group on Facebook
        * John wants to start porting Ollama.ai to Perl
        * Ollama is basically a Golang server that exposes a JSON API that bridges lots of different LLMs and makes local dev easy
        * Ollama client is pretty straightforward to write
        * Ollama may be able to replace Oobabooga, but we would be relacing one non-Perl software with another non-Perl software?
        * Need to discuss in more depth and look at actual code etc

    * Competing with Python
        * Need ported to Perl:
            * OpenAI
            * LangChain
            * Autogen
            * Oobabooga
            * LlamaIndex
            * Ollama

* New Business
    * none

* Closing, 2:56pm
