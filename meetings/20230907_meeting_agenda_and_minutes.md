# AI Perl Committee
# Meeting Agenda & Minutes

## September 7th, 2023
## Third Official Meeting

* Opening, 2:01pm

* Attendance & Introductions (already done in preceding meeting)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Zakariyya Mughal (biomedical imaging, BioPerl, PDL)
    * Christos Argyropoulos (kidney physician, genomic & biomarker discovery, big data sets, EHR, methodology development for sequencing, new markers for kidney disease)
    * John Napiorkowski (involved in Perl since 90's, helping Perl make people happy and making Perl competitive)
    * Adam Russell (computer scientist, dissertation in computational geometry in Perl, leads group United Health Group subsidiary OptumAI, business AI automating building processes & reviewing documents)
    * Antanas Vaitkus (Lithuania, informatics & bioinformatics, crystallography, open database software in Perl)
    * John Kirk (no intro, microphone problems)

* Announcements
    * Meet every 2 weeks on Thursday 2:00-3:00pm Central time zone
        * Immediately following Science Perl Committee and Perl::Types Committee meetings
    * Chairman John Napiorkowski present but sore throat, requests 1st Vice Chairman Will Braswell to preside

* Previous Meeting's Minutes
    * 8/24/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell, seconded by Antanas Vaitkus to be accepted as read, voted unanimously

* Old Business

    * TensorFlow Phase 2, TPF grant proposal
        * Still waiting for feedback from TPF board & president
        * Will Braswell needs to ping TPF President Stuart Mackintosh to move forward
        * John Napiorkowski says TPF grant committee is out of their league on AI grant proposals
        * Christos asks about using crowdfunding, John Napiorkowski says we will form a Professional Perl Association if TPF can't provide funding

    * PerlGPT Phase 1, TPF grant re-proposal
        * John Napiorkowski has not received any reply from TPF whatsoever, will contact TPF about it

    * TPF Grants Committee population
        * Need knowledgeable volunteers to join the committee
        * Can not be officer in AI Perl Committee while also on TPF Grants Committee, would be conflict of interest
        * Christos asked how many grants are given every year, other than Dave Mitchell & Tony Cook the answer is usually 0

    * Monthly AI Perl Hackathon
        * First hackathon held last night, it was a good success, about a dozen people total
        * Machine vision focus
        * Zaki gives report about hackathon, use previous Perl image classification demo to create new muti-object recognition demo, got all the way to TensorFlow neural network returning output to Perl
        * Zaki is working on high-level interface now, maybe have something out in about a month
        * Christos asks if PDL is using FFI or XS, Zaki says we are using FFI Platypus with interesting wrappers around FFI to make it easier, use code generation to automatically create FFI such as arrays-of-structs with pre-allocated memory, etc
        * Video recording of last night is available to AI Perl Committee members
        * Will Braswell claims Zaki Mughal is the smartest AI Perl programmer in the world, Zaki objects due to all his advance preparation, minutes amended to say Zaki APPEARS to be the smartest AI Perl programmer in the world
        * 10/4/23 7-9pm, next AI Perl Hackathon, Machine Vision P2

    * Navi AI
        * Currently marketing Navi v1.5 including LLaMa v2 and TensorFlow v2
        * Machine vision upgrade coming soon from last night's hackfest, need to finish multi-object recognition and use to upgrade Navi's visual abilities
        * AI API upgrade coming soon as well

* New Business

    * New Business
        * none

* Closing, 2:44pm
