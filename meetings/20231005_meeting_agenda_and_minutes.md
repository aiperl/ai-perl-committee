# AI Perl Committee
# Meeting Agenda & Minutes

## October 5th, 2023
## Fifth Official Meeting

* Opening, 2:08pm

* Attendance & Introductions (already done in preceding meeting)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Yussuf Arifin AKA Zahirul Haq (from Asia, new to coding, wants to learn more about Perl)
    * Jakub Pawlowski (wants to integrate Perl AI with .NET & Mono)
    * Justin Kelly (web developer from Dublin, wants to get back into Perl, started w/ Perl CGI years ago, loves Perl)
    * John Napiorkowski (involved in Perl since 90's, helping Perl make people happy and making Perl competitive)

* Announcements
    * Chairman John Napiorkowski requests 1st Vice Chairman Will Braswell to preside
    * Meet every 2 weeks on Thursday 2:00-3:00pm Central time zone
        * Immediately following Science Perl Committee and Perl::Types Committee meetings

* Previous Meeting's Minutes
    * 9/21/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell, seconded by John Napiorkowski to be accepted as read, voted unanimously

* Old Business

    * TensorFlow
        * Phase 2, TPF grant proposal
            * Not approved by TPF Grant Committee, based on incomplete information, further information provided by Zaki but no appeal accepted
            * Still waiting for feedback from TPF board & president
        * Zaki has released v0.0.7, now including Object Detection neural network
            * https://metacpan.org/dist/AI-TensorFlow-Libtensorflow/view/lib/AI/TensorFlow/Libtensorflow/Manual/Notebook/InferenceUsingTFHubCenterNetObjDetect.pod
            * https://nbviewer.org/github/EntropyOrg/perl-AI-TensorFlow-Libtensorflow/blob/master/notebook/InferenceUsingTFHubCenterNetObjDetect.ipynb

    * PerlGPT
        * Phase 1, TPF grant re-proposal
            * Proposal published 9/20/23, 2-week public comment phase now complete, TPF Grant Committee should make a decision w/in next 2 weeks
            * https://news.perlfoundation.org/post/perlgptphase1

    * AI Perl Hackathon
        * Every 6 weeks
        * Next event 10/18/23, 7-9pm Central time zone, theme is Machine Vision P2

    * Navi AI
        * Navi v1.5 whitepaper now published
            * https://chatgpu.ai/pages/navi-ai
        * Still working on refactoring Camel (Catalyst MVC) to include API, consciousness run loop, etc.
        * Zaki helped set up Docker for dev vs pre-prod

* New Business

    * Financial support
        * Commercial via ChatGPU & APTech
        * Non-commercial via new unreleased tax-exempt org

    * New unreleased AI & science software component
        * Micro grant already received & disbursed
        * Software development is underway
        * Will be used as part of Navi's world map

* Closing, 2:30
