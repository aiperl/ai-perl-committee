# AI Perl Committee
# Meeting Agenda & Minutes

## November 2nd, 2023
## Seventh Official Meeting

* Opening, 2:02pm

* Attendance & Introductions (already done in preceding meeting)
    * John Napiorkowski (involved in Perl since 90's, helping Perl make people happy and making Perl competitive)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Yussuf Arifin AKA Zahirul Haq (from Malaysia, new to coding, wants to learn more about Perl)
    * Jakub Pawlowski (wants to integrate Perl AI with .NET & Mono)

* Announcements
    * Chairman John Napiorkowski requests 1st Vice Chairman Will Braswell to preside
    * Meet every 2 weeks on Thursday 2:00-3:00pm Central time zone
        * Immediately following Science Perl Committee and Perl::Types Committee meetings

* Previous Meeting's Minutes
    * 10/19/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell, seconded by John Napiorkowski to be accepted as read, voted unanimously

* Old Business

    * TensorFlow
        * Phase 2, TPF grant proposal
            * Still waiting for response from TPF president, promised 10 weeks ago, presumably they are purposefully misleading or delaying
        * Need to work with Zaki to create new Docker for Object Detection neural network

    * PerlGPT
        * Phase 1, TPF grant re-proposal
            * https://news.perlfoundation.org/post/perlgptphase1
            * Proposal published 9/20/23, 2-week public comment phase now complete, TPF Grant Committee was supposed to have already voted
            * No update, still waiting for Saif
            * John will ping Saif

    * AI Perl Hackathon
        * Every 6 weeks
        * Next event 11/29/23, SPECIAL TIME 8:30-9:30pm Central time zone, theme is Machine Vision P2?
            * Date & time are currently subject to change due to holidays etc.

    * Navi AI
        * John & Will are refactoring Camel (Catalyst MVC) to break out Navi consciousness run loop
        * Navi robot prototype currently being designed, will review specifications at next meeting

    * Financial support
        * 3 tiers of Perl organizations
            * Commercial via ChatGPU & APTech
            * Mutual aid society via Perl Professional Association
            * Non-profit via Perl Labs
        * Will & John can pursue Navi Kickstarter after we hear back from TPF Grant Committee on PerlGPT proposal

    * New unreleased AI & science software component
        * Currently under development
        * Component of Navi's world map
        * Alien interface running now, moving on to the main API soon

* New Business
    * OpenAI::API
        * OpenAI::API on CPAN is currently behind, does not support streaming
        * John suggests using futures, would be compatible w/ Mojolicious
        * OpenAI pushing Python too much, need to push back with Perl
        * Nelson Ferraz is the original author, no update since April
        * Will has received no response from Nelson since May
        * John will ping Nelson

* Closing, 2:35pm
