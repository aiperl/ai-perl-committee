# AI Perl Committee
# Meeting Agenda & Minutes

## June 13th, 2024
## Twenty-Third Official Meeting

* Opening, 3:02pm

* Attendance & Introductions (already done in preceding meeting)
    * John Napiorkowski (maintainer of Catalyst)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Robbie Hatley (EE & computer programming in Perl)
    * Mickey Macten (online security and streaming multimedia)

* Announcements
    * Chairman John Napiorkowski requests 1st Vice Chairman Will Braswell to preside
    * Meet every 2 weeks on Thursday 2:00-3:00pm Central time zone
        * Immediately following Science Perl Committee and Perl::Types Committee meetings

* Previous Meeting's Minutes
    * 5/30/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Robbie Hatley to be accepted as read, voted unanimously
    * NEED SPECIAL BYLAW to allow meeting minutes approval with minimum of one officer only, not full voting quorum

* Old Business

    * Website  [ NO CHANGE 20240613 ]
        * perlcommunity.org/ai

    * TensorFlow  [ NO CHANGE 20240613 ]
        * RoboPerl nonprofit grant, approved for these 3 TensorFlow projects:
            * Object Detection
                * Need to work on getting Docker integrated into Navi AI
            * Speech Recognition
                * https://gitlab.com/zmughal/notebook-perl-libtensorflow-speech-recognition
                * Need new programmer
            * Optical Character Recognition
                * Not started yet
        * STILL TRYING TO WORK WITH POTENTIAL NEW PROGRAMMER

    * PerlGPT
        * Phase 1, TPF grant re-proposal, initially approved, NOW DECLINED
            * Sent first official grant report to Saif at TPF, received initial reply complaining about code authorship
            * Still waiting for official TPF grant committee to vote for continued grant approval
            * John & Will attend TPF Community Reps meeting on 4/19, asked Saif for update, only told to wait 2 more weeks until voting
            * TPF Grant Committee Chair Saif Ahmed wrote very rude/angry e-mail to John after we asked for them to provide the long-promised vote
            * TPF Grant Committee is clearly not interested in actually helping AI development
            * TPF Grant declined, we will no longer support the TPF Grant Committee
        * Repositories
            * https://gitlab.com/aiperl/perlgpt
            * https://gitlab.com/aiperl/perlgpt-working-tmp
        * We are using the MetaCPAN API directly
            * Search through all CPAN for good training distributions
            * Must be recent, last 3 - 5 years to start, add older if needed
            * Must pass CPAN Testers with high pass rates, 90% or higher
            * Must pass CPANTS Kwalitee with high score, 25 out of 30 or higher
            * Automatically annotate Perl::Critic violations with high severity, 4 out of 5 or higher
            * Somehow give preference to well-written code containing descriptive comments?
        * Current issues
            * Even more memory leaks
                * 'Number-ZipCode-JP'  => 'causes memory leak, presumably due to long Kanji and/or regex strings',
                * 'Jacode'             => 'causes memory leak, presumably due to long Kanji and/or regex strings',
                * 'Jacode4e'           => 'MAYBE causes memory leak, presumably due to long Kanji and/or regex strings',
                * 'Jacode4e-RoundTrip' => 'MAYBE causes memory leak, presumably due to long Kanji and/or regex strings',
                * 'Lingua-EN-Opinion'  => 'causes memory leak, presumably due to long wordlist data structures in lib/Lingua/EN/Opinion/Emotion.pm',
            * High memory usage distributions AKA memory hogs
                * 'Chart-Plotly'       => 'MAYBE runs long & hogs memory, presumably due to long number lists in examples/traces/volume.pl etc',
                * 'HiPi'               => 'runs long & hogs memory, presumably due to long fonts in lib/HiPi/Graphics/BitmapFont/MonoEPD102.pm etc',
                * 'Date-Manip'         => 'runs long & hogs memory, presumably due to long international characters in lib/Date/Manip/DM5.pm etc',
                * 'Games-Axmud'        => 'runs long & hogs memory, presumably due to long file lib/Games/Axmud/Cmd.pm etc',
                * 'Paws'               => 'runs long & hogs memory, presumably due to large number of files',
            * MetaCPAN API Filter Bug
                * the 'filter' parameter below has some unknown issues,
                    * `filter => { term => { status => 'latest' }},  # only return latest versions of each distribution`
                * on 20240604 'Author-Daemon-Site-Ptr-Bond' was returned twice in the same API call
                * both Author-Daemon-Site-Ptr-Bond-0.008.tar.gz and Author-Daemon-Site-Ptr-Bond-0.004.tar.gz
                * this caused the accepted + rejected distributions to be 4999 instead of 5000, and possibly other undetected secondary effects
                * More info in my code comments
                    * https://gitlab.com/aiperl/perlgpt/-/commit/3099e3b1ecf3c262bca2fef0c49b8f60d3aac699#b35c7fa05a28c5da3d70bbc98b1229a5a0883861_416_421
                * NEED FILE METACPAN BUG REPORT?
                    * Will says the MetaCPAN API should be able to detect bad or duplicate metadata and filter correctly
                * NEED INFORM AUTHOR OF BAD METADATA?
                    * John says it is caused by bad metadata in one or both of those release files
            * Number-ZipCode-JP
                * Distribution contains Kanji characters and huge Perl regexes
                * Causes memory leak in Perl::Tidy or Perl::Critic or PPI
                * Even Parallel::ForkManager can't contain it!
                * Disabled for now, may not be possible to debug memory leak until after new Perl VM exists
            * UTF8
                * Perl::Critic crashes on several CPAN distributions with files containing special characters
                    * Can't parse code: Encountered unexpected character '226'
                    * https://fastapi.metacpan.org/source/PEVANS/Syntax-Operator-In-0.09/t/02elem-numbery.t
                * Existing PPI and/or Perl::Critic issue(s)
                    * https://github.com/Perl-Critic/PPI/issues/22
                    * https://github.com/Perl-Critic/Perl-Critic/issues/562
            * Scroll API
                * Perl code showing the scroll API working
                    * https://github.com/metacpan/metacpan-examples/blob/main/scripts/author/1b-scroll-all-authors-es.pl
                * Ruby code showing the scroll API working
                    * https://github.com/librariesio/libraries.io/pull/2817/files
            * Perl::Critic
                * Policy::Module::RequireEndWithOne
                    * NEED INVESTIGATE POSSIBLE BUG, false violation on Build.PL files which are not actually Perl modules?
        * Current status
            * Have draft LLM.md file containing target LLM training template
            * Organize into granularity of less than 4K tokens per collection of code + docs
                * Distribution
                * Files
                * Packages / Classes
                * Subroutines
            * Currently working on phase 5 to populate LLM.md template
            * Refactored file I/O to includes fork-safe error checking
            * Made list of all Kwalitee indicators for future fine-tuning of acceptance/rejection criteria
            * PerlGPT MetaCPAN Curator paper mostly completed for Science Perl Journal, reviewed by Dr. Marc Perry
                * Updated the comments 
                * Found new memory leaks, memory hogs, and API filter bug

    * AI Perl Hackathon
        * Every 6 weeks alternate between virtual only and hybrid event
        * Next event 7/3/24, SPECIAL DATE, 7:00-9:00pm Central time zone, theme is PerlGPT or TensorFlow
            * SPECIAL IN-PERSON & VIRTUAL HYBRID EVENT
            * Diogenes Hackerspace, 13800 Dragline Dr, Austin, TX
            * Meetup Event, please RSVP
                * https://www.meetup.com/austin-perl-mongers/events/301067468/
        * Next next event 8/7/24, 7:00-9:00pm Central time zone, theme is PerlGPT or TensorFlow
            * Virtual only

    * Navi AI  [ NO CHANGE 20240613 ]
        * John & Will got initial refactoring Camel (Catalyst MVC) running, need refactor webcam code w/ Zaki
        * Need Robert Grimes to help get Navi Dockers running locally for John, so he can continue Camel upgrades
        * STILL TRYING TO GET NEW CATALYST PROGRAMMER

    * Navi Robot
        * Navi single-arm robot prototype currently being tested at Diogenes Hackerspace
        * Potential business interest from Hackerspace, trying to find early adoption customers in Austin, will provide more details as available
        * Purchased airline approved carry-on 100% aluminum protective case, with pick-and-pluck foam
            * Will have to remove wheels and fold down camera to fit inside maximum allowable size of airline restrictions
        * Purchased bluetooth keyboard & touchpad combo, Logitech K400
        * STILL TRYING TO DEBUG THE RANDOM BOOT ISSUE
            * 3 beeps at end of boot sequence indicates proper functionality
            * lack of 3 beeps allows tablet app to connect & view cameras, but not control arm or wheels
            * Need purchase powered USB hub to test possible power distribution bug
            * Need to contact manufacturer or other robot developers
 
    * Cowl API  [ NO CHANGE 20240613 ]
        * Currently under development, component of Navi's world map
        * RDF::Cowl v1.0.0 released for Valentine's Day!
            * https://metacpan.org/dist/RDF-Cowl
            * https://www.facebook.com/machinelearningperl/posts/pfbid02HNfoDG2E75yszSCVR2E8WHKDbqprknv2afSkuPgeLKKPnL3k91PbXVyNgVRCHNGPl
        * Next steps
            * Alien::Robot
                * http://robot.obolibrary.org/
            * iPerl notebook for gene ontology using Cowl, and eventually all following components
            * Attean / SPARQL (has in-memory triple store)
            * SQLite or other specific triple-store software (to provide on-disk triple store)
                * https://github.com/INCATools/semantic-sql
            * CWPK & KB
            * Docker
            * Navi integration
        * NEED INTEGRATE INTO NAVI AFTER CATALYST REFACTOR
        * NEED MAYBE WORK WITH Adam Russell after Science Track papers are done

    * OpenAI::API  [ NO CHANGE 20240613 ]
        * Need completely-compatible OpenAI client, collaborating with Ryan and John
        * Ryan Voots is developing OpenAI-compatible API using low-level Perl libraries on top of Python LLM loading/inference
        * Ryan is working on SSE already, we will let him show us his SSE and tell him if we like it
        * When Ryan has something ready to test, John can test it and get back with results
        * NEED UPDATE FROM RYAN

    * LangChain  [ NO CHANGE 20240613 ]
        * John wants to start porting LangChain to Perl
        * We will use our prototype Python-to-Perl translator to get started, then invite others to join the project
        * Repositories
            * https://gitlab.com/aiperl/langchain
            * https://gitlab.com/aiperl/langchain-perl

    * Ollama  [ NO CHANGE 20240613 ]
        * From AI Perl Committee group on Facebook
        * John wants to start porting Ollama.ai to Perl
        * Ollama is basically a Golang server that exposes a JSON API that bridges lots of different LLMs and makes local dev easy
        * Ollama client is pretty straightforward to write
        * Ollama may be able to replace Oobabooga, but we would be relacing one non-Perl software with another non-Perl software?
        * Need to discuss in more depth and look at actual code etc

    * BigCode
        * LLM training procedures & datasets for many programming languages
        * They take all their code from GitHub w/out any curating
        * Will says this is a garbage training procedure
        * NEED REPLY FROM BIGCODE
            * ASKED DR. CHRISTOS TO USE ACADEMIC E-MAIL ACCOUNT
            * What is the training data format?
            * Will they give us free GPU training time?

    * Code Llama Group (Meta?)  [ NO CHANGE 20240613 ]
        * NEED JOHN TO FIND OUT, same as BigCode above

    * Hugging Face  [ NO CHANGE 20240613 ]
        * GPUZero
            * Not for training or fine-tuning, only for running inference on pre-existing LLMs
            * Does not help us traing PerlGPT

    * Competing with Python  [ NO CHANGE 20240613 ]
        * Need ported to Perl:
            * OpenAI
            * LangChain
            * Autogen
            * Oobabooga
            * LlamaIndex
            * Ollama
            * BigCode

    * TPC Science Track Talks  [ NO CHANGE 20240613 ]
        * GenProRegEx 
            * Dr. Adam Russell's previous 2024 paper
            * CANCELLED by his company, archived in OJS
            * Uses genetic programming to learn a regular expression which will extract text entities based on examples
                * Give it examples of phone numbers, it will generate regular expressions to match the phone numbers in training set
        * Structure Based Structuring of Unstructured Data
            * Dr. Adam Russell's current 2024 paper
            * Uses LLMs to produce RDF & OWL output
        * PerlGPT paper
            * Will & John will partner on PerlGPT presentation
            * NEED SEND COPY TO JOHN, he will review and submit small additional content
        * Reasoning About the Rigor of Perl Programs
            * George Baugh author
            * Wants to use AI::Categorizer

    * TPC Normal Track Talks
        * Will got approved for a talk "The State of the Onion AI"
        * JOHN WILL COVER OPENAI CHATGPT LLM USAGE

    * Genetic Programming  [ NO CHANGE 20240613 ]
        * Adam Russell working on this right now
        * NEED MORE INFO & DISCUSSION

    * TPF Prospectus Document  [ NO CHANGE 20240613 ]
        * TPF requested Will to provide an AI Perl write-up for their prospectus, did provide along with website link
        * TPF admin assistant Amber Krawczyk accepted and included as part of 2024 prospectus

    * RoboPerl Nonprofit  [ NO CHANGE 20240613 ]
        * NEED CREATE BUSINESS PLAN FOR NONPROFIT

* New Business

    * Marketing Subcommittee
        * Will and Dr. Christos as potential subcommittee members
        * AI Perl Committee prospectus
        * AI Perl Committee business plan

    * Fundraising Subcommittee
        * John as potential subcommittee member
        * Reach out to companies using Perl
        * Punch list of what to do with the money
        * TPF always thinks too small
        * Raise $1M at least
        * Need to give the companies something they want
            * They have a hard time hiring Perl programmers, so we provide documentation and training to help Perl programmers
        * People donate to nonprofits for self interest reasons, they want to meet a celebrity or get a physical product or some other tangible benefit
        * Fundraising events need to be good networking activities
        * Work with academic groups & individuals to find grants, local and state and national government funding

    * Advisory Subcommittee
        * Determines how to spend the funds received by Fundraising Subcommittee
        * Groups providing funds get to have a seat on this subcommittee

    * 1st Annual Perl Committee (Constitutional!) Convention
        * During TPC in Las Vegas, Tuesday 6/25/2024, 7-9pm PACIFIC TIME ZONE
        * Hybrid event, both in-person and online
        * Suggested Officers
            * Chairman: John Napiorkowski
            * 1st Vice: Dr. Christos Argyropoulos
            * 2nd Vice: Dr. Adam Russell
            * Secretary & Treasurer: Will Braswell

* Closing, 4:16pm
