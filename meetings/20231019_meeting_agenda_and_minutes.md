# AI Perl Committee
# Meeting Agenda & Minutes

## October 19th, 2023
## Sixth Official Meeting

* Opening, 2:08pm

* Attendance & Introductions (already done in preceding meeting)
    * John Napiorkowski (involved in Perl since 90's, helping Perl make people happy and making Perl competitive)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Yussuf Arifin AKA Zahirul Haq (from Malaysia, new to coding, wants to learn more about Perl)
    * Jakub Pawlowski (wants to integrate Perl AI with .NET & Mono)
    * Minas Polychronidis (in Greece, owns think.gr software company)

* Announcements
    * Chairman John Napiorkowski requests 1st Vice Chairman Will Braswell to preside
    * Meet every 2 weeks on Thursday 2:00-3:00pm Central time zone
        * Immediately following Science Perl Committee and Perl::Types Committee meetings

* Previous Meeting's Minutes
    * 10/5/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell, seconded by John Napiorkowski to be accepted as read, voted unanimously

* Old Business

    * TensorFlow
        * Phase 2, TPF grant proposal
            * Will sent another e-mail to TPF board & president requesting response promised 8 weeks ago
        * Zaki has released v0.0.7, now including Object Detection neural network
            * Need get new Docker for object detection

    * PerlGPT
        * Phase 1, TPF grant re-proposal
            * https://news.perlfoundation.org/post/perlgptphase1
            * Proposal published 9/20/23, 2-week public comment phase now complete, TPF Grant Committee voting will start soon
            * John received an e-mail from Saif on 10/18/23 saying there was a delay in the October grant process, they should be voting by next week

    * AI Perl Hackathon
        * Will & John had mini-hackathon last night, continuing Navi AI refactoring (see below)
        * Every 6 weeks
        * Next event 11/29/23, SPECIAL TIME 8:30-9:30pm Central time zone, theme is Machine Vision P2???
            * Date & time are currently subject to change due to holidays etc.

    * Navi AI
        * ChatGPU.ai catalog pricing options now updated
        * John & Will are refactoring Camel (Catalyst MVC) to break out Navi consciousness run loop

* New Business

    * Financial support
        * Commercial via ChatGPU & APTech
        * Non-commercial via forthcoming tax-exempt org
        * John sees that "Python is eating our lunch" in new AI genre in the past 1 year, this is an opportunity for Perl to get back in the game, if TPF doesn't have money then we should ask other Perl companies to help, Python code is just dumb wrappers around text processing and pre-written prompts, there's no reason why Perl shouldn't be able to make it happen
        * Will & John can pursue Navi Kickstarter after we hear back from TPF Grant Committee on PerlGPT proposal

    * New unreleased AI & science software component
        * Micro grant has been disbursed to Perl programmer
        * Development should now be starting

* Closing, 2:38
