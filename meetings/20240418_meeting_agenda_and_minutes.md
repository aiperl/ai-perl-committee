# AI Perl Committee
# Meeting Agenda & Minutes

## April 18th, 2024
## Nineteenth Official Meeting

* Opening, 2:18pm

* Attendance & Introductions (already done in preceding meeting)
    * John Napiorkowski (maintainer of Catalyst)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Adam Russell, PhD (computer scientist, leads group United Health Group subsidiary OptumAI, developing new products to bring AI & ML to admin workers)
    * Mickey Macten (online security and streaming multimedia)

* Announcements
    * Chairman John Napiorkowski requests 1st Vice Chairman Will Braswell to preside
    * Meet every 2 weeks on Thursday 2:00-3:00pm Central time zone
        * Immediately following Science Perl Committee and Perl::Types Committee meetings

* Previous Meeting's Minutes
    * 4/4/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by John Napiorkowski to be accepted as read, voted unanimously

* Old Business

    * TensorFlow [ NO CHANGE 20240418 ]
        * RoboPerl nonprofit grant, approved for these 3 TensorFlow projects:
            * Object Detection
                * Need to work on getting Docker integrated into Navi AI
            * Speech Recognition
                * https://gitlab.com/zmughal/notebook-perl-libtensorflow-speech-recognition
                * Found somebody to replace Zaki, hopefully!
            * Optical Character Recognition
                * Not started yet

    * PerlGPT
        * Phase 1, TPF grant re-proposal, approved
            * Sent first official grant report to Saif at TPF, received initial reply complaining about code authorship
            * Still waiting for official TPF grant committee to vote for continued grant approval
            * NEED REPLY FROM SAIF
        * Repositories
            * https://gitlab.com/aiperl/perlgpt
            * https://gitlab.com/aiperl/perlgpt-working-tmp
        * We are using the MetaCPAN API directly
            * Search through all CPAN for good training distributions
            * Must be recent, last 3 - 5 years to start, add older if needed
            * Must pass CPAN Testers with high pass rates, 90% or higher
            * Must pass CPANTS Kwalitee with high score, 25 out of 30 or higher
            * Automatically annotate Perl::Critic violations with high severity, 4 out of 5 or higher
            * Organize into granularity of less than 4K tokens per collection of code + docs
                * Distribution
                * Files
                * Packages / Classes
                * Subroutines
            * Currently creating LLM.md file containing target LLM training template
            * Somehow give preference to well-written code containing descriptive comments?
            * Current issues
                * UTF8
                    * Perl::Critic crashes on several CPAN distributions with files containing special characters
                        * Can't parse code: Encountered unexpected character '226'
                        * https://fastapi.metacpan.org/source/PEVANS/Syntax-Operator-In-0.09/t/02elem-numbery.t
                    * Existing PPI and/or Perl::Critic issue(s)
                        * https://github.com/Perl-Critic/PPI/issues/22
                        * https://github.com/Perl-Critic/Perl-Critic/issues/562
                * Scroll API
                    * Perl code showing the scroll API working
                        * https://github.com/metacpan/metacpan-examples/blob/main/scripts/author/1b-scroll-all-authors-es.pl
                    * Ruby code showing the scroll API working
                        * https://github.com/librariesio/libraries.io/pull/2817/files
                * Perl::Critic
                    * Policy::Module::RequireEndWithOne
                        * NEED INVESTIGATE POSSIBLE BUG, false violation on Build.PL files which are not actually Perl modules?

    * AI Perl Hackathon [ NO CHANGE 20240418 ]
        * Every 6 weeks alternate between virtual only and hybrid event
        * Next event 5/15/24, 7:00-9:00pm Central time zone, theme is PerlGPT or TensorFlow
            * Virtual only
        * Next next event 7/3/24, SPECIAL DATE, 7:00-9:00pm Central time zone, theme is PerlGPT or TensorFlow
            * SPECIAL IN-PERSON & VIRTUAL HYBRID EVENT
            * Diogenes Hackerspace, 13800 Dragline Dr, Austin, TX
            * NEED CONTACT ROB FOR HACKERSPACE RESERVATION

    * Navi AI & Robot
        * John & Will got initial refactoring Camel (Catalyst MVC) running, need refactor webcam code w/ Zaki
        * Need Robert Grimes to help get Navi Dockers running locally for John, so he can continue Camel upgrades
        * Navi single-arm robot prototype currently being tested at Diogenes Hackerspace
        * Potential business interest from Hackerspace, trying to find early adoption customers in Austin, will provide more details as available
        * NEED CONFIRM NEW CATALYST PROGRAMMER
        * NEED SCHEDULE WORK SESSION WITH JOHN FOR CATALYST REFACTOR
 
    * Cowl API [ NO CHANGE 20240418 ]
        * Currently under development, component of Navi's world map
        * RDF::Cowl v1.0.0 released for Valentine's Day!
            * https://metacpan.org/dist/RDF-Cowl
            * https://www.facebook.com/machinelearningperl/posts/pfbid02HNfoDG2E75yszSCVR2E8WHKDbqprknv2afSkuPgeLKKPnL3k91PbXVyNgVRCHNGPl
        * Next steps
            * Alien::Robot
                * http://robot.obolibrary.org/
            * iPerl notebook for gene ontology using Cowl, and eventually all following components
            * Attean / SPARQL (has in-memory triple store)
            * SQLite or other specific triple-store software (to provide on-disk triple store)
                * https://github.com/INCATools/semantic-sql
            * CWPK & KB
            * Docker
            * Navi integration
        * NEED INTEGRATE INTO NAVI AFTER CATALYST REFACTOR
        * NEED MAYBE WORK WITH Adam Russell after Science Track papers are done

    * OpenAI::API  [ NO CHANGE 20240418 ]
        * Need completely-compatible OpenAI client, collaborating with Ryan and John
        * Ryan Voots is developing OpenAI-compatible API using low-level Perl libraries on top of Python LLM loading/inference
        * Ryan is working on SSE already, we will let him show us his SSE and tell him if we like it
        * When Ryan has something ready to test, John can test it and get back with results
        * NEED UPDATE FROM RYAN

    * LangChain  [ NO CHANGE 20240418 ]
        * John wants to start porting LangChain to Perl
        * We will use our prototype Python-to-Perl translator to get started, then invite others to join the project
        * Repositories
            * https://gitlab.com/aiperl/langchain
            * https://gitlab.com/aiperl/langchain-perl

    * Ollama  [ NO CHANGE 20240418 ]
        * From AI Perl Committee group on Facebook
        * John wants to start porting Ollama.ai to Perl
        * Ollama is basically a Golang server that exposes a JSON API that bridges lots of different LLMs and makes local dev easy
        * Ollama client is pretty straightforward to write
        * Ollama may be able to replace Oobabooga, but we would be relacing one non-Perl software with another non-Perl software?
        * Need to discuss in more depth and look at actual code etc

    * BigCode
        * LLM training procedures & datasets for many programming languages
        * They take all their code from GitHub w/out any curating
        * Will says this is a garbage training procedure
        * NEED JOHN TO FIND OUT
            * What is the training data format?
            * Will they give us free GPU training time?
            * Can we join w/out academic credentials
                * We will bring on Dr. Christos and Dr. Russell etc if needed

    * Code Llama Group (Meta?)
        * NEED JOHN TO FIND OUT, same as BigCode above

    * Competing with Python  [ NO CHANGE 20240418 ]
        * Need ported to Perl:
            * OpenAI
            * LangChain
            * Autogen
            * Oobabooga
            * LlamaIndex
            * Ollama
            * BigCode

    * Website  [ NO CHANGE 20240418 ]
        * perlcommunity.org/ai

    * Science Track Talks
        * GenProRegEx (placeholder name, NEED REPLACE)
            * Dr. Adam Russell's current 2024 paper
            * Uses genetic programming to learn a regular expression which will extract text entities based on examples
                * Give it examples of phone numbers, it will generate regular expressions to match the phone numbers in training set
        * PerlGPT paper
            * Will & John will partner on PerlGPT presentation
            * John will ask Vincent to be a co-author as well

* New Business

    * Genetic Programming
        * Adam Russell working on this right now
        * NEED MORE INFO & DISCUSSION

* Closing, 2:51pm
