# AI Perl Committee
# Meeting Agenda & Minutes

## September 21st, 2023
## Fourth Official Meeting

* Opening, 2:00pm

* Attendance & Introductions (already done in preceding meeting)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Antanas Vaitkus (Lithuania, background in bioinformatics, working in crystallography for 10 years, open database software in Perl to collect all small molecule structures)
    * Justin Kelly (web developer from Dublin, wants to get back into Perl, started w/ Perl CGI years ago, loves Perl)
    * Robbie Hatley (EE & computer programming in Perl for 8 years)
    * Venkataramana Mokkapati AKA "Ramana" (works for large semiconductor company, build CPAN-like repository of EE components, elliptical curve cryptography)
    * Yussuf Arifin AKA Zahirul Haq (from Asia, new to coding, wants to learn more about Perl)
    * Jakub Pawlowski (wants to integrate Perl AI with .NET & Mono) 

* Announcements
    * Meet every 2 weeks on Thursday 2:00-3:00pm Central time zone
        * Immediately following Science Perl Committee and Perl::Types Committee meetings
    * Chairman John Napiorkowski unable to attend, requests 1st Vice Chairman Will Braswell to preside

* Previous Meeting's Minutes
    * 9/7/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell, seconded by Antanas Vaitkus to be accepted as read, voted unanimously

* Old Business

    * TensorFlow
        * Phase 2, TPF grant proposal
            * Still waiting for feedback from TPF board & president
        * Zaki continues to make progress on Object Detection neural network

    * PerlGPT
        * Phase 1, TPF grant re-proposal
            * John Napiorkowski has received reply from TPF, Code Llama update submitted, updated proposal will be published soon

    * AI Perl Hackathon
        * Create regular event
            * Repeat every 6 weeks
            * Next event 10/18/23, 7-9pm Central time zone, theme is Machine Vision P2
            * Moved by Will Braswell, seconded by Robbie Hatley, voted (nearly?) unanimously

    * Navi AI
        * Navi v1.5 whitepaper presented by Will Braswell
        * Ramana Mokkapati asked about the next steps in Navi development and the associated costs
        * Will Braswell described the refactoring process of Camel (Catalyst MVC) to include API, consciousness run loop, etc.
        * Estimated cost is $5K to $10K to complete Navi v2.0 refactoring project

* New Business
    * none

* Closing, 3:00
