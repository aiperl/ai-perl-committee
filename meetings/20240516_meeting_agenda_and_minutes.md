# AI Perl Committee
# Meeting Agenda & Minutes

## May 16th, 2024
## Twenty-First Official Meeting

* Opening, 2:07pm

* Attendance & Introductions (already done in preceding meeting)
    * John Napiorkowski (maintainer of Catalyst)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Robbie Hatley (EE & computer programming in Perl)
    * Joshua Oliva (interested in scientific breakthroughs, physics, health topics)
    * Josh Rabinowitz

* Announcements
    * Chairman John Napiorkowski requests 1st Vice Chairman Will Braswell to preside
    * Meet every 2 weeks on Thursday 2:00-3:00pm Central time zone
        * Immediately following Science Perl Committee and Perl::Types Committee meetings

* Previous Meeting's Minutes
    * 5/2/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by John Napiorkowski to be accepted as read, voted unanimously
    * NEED SPECIAL BYLAW to allow meeting minutes approval with minimum of one officer only, not full voting quorum

* Old Business

    * Website  [ NO CHANGE 20240516 ]
        * perlcommunity.org/ai

    * TensorFlow  [ NO CHANGE 20240516 ]
        * RoboPerl nonprofit grant, approved for these 3 TensorFlow projects:
            * Object Detection
                * Need to work on getting Docker integrated into Navi AI
            * Speech Recognition
                * https://gitlab.com/zmughal/notebook-perl-libtensorflow-speech-recognition
                * Need new programmer
            * Optical Character Recognition
                * Not started yet
        * STILL TRYING TO WORK WITH POTENTIAL NEW PROGRAMMER

    * PerlGPT
        * Phase 1, TPF grant re-proposal, initially approved
            * Sent first official grant report to Saif at TPF, received initial reply complaining about code authorship
            * Still waiting for official TPF grant committee to vote for continued grant approval
            * John & Will attend TPF Community Reps meeting on 4/19, asked Saif for update, only told to wait 2 more weeks until voting
            * TPF Grant Committee Chair Saif Ahmed wrote very rude/angry e-mail to John after we asked for them to provide the long-promised vote
            * TPF Grant Committee is clearly not interested in actually helping AI development
            * FOREVER WAITING FOR TPF GRANT COMMITTEE VOTE, CURRENTLY ASSUMING THEY WILL REJECT US AND WE SHOULD MOVE ON
        * Repositories
            * https://gitlab.com/aiperl/perlgpt
            * https://gitlab.com/aiperl/perlgpt-working-tmp
        * We are using the MetaCPAN API directly
            * Search through all CPAN for good training distributions
            * Must be recent, last 3 - 5 years to start, add older if needed
            * Must pass CPAN Testers with high pass rates, 90% or higher
            * Must pass CPANTS Kwalitee with high score, 25 out of 30 or higher
            * Automatically annotate Perl::Critic violations with high severity, 4 out of 5 or higher
            * Somehow give preference to well-written code containing descriptive comments?
        * Current issues
            * Number-ZipCode-JP
                * Distribution contains Kanji characters and huge Perl regexes
                * Causes memory leak in Perl::Tidy or Perl::Critic or PPI
                * Even Parallel::ForkManager can't contain it!
                * Disabled for now, may not be possible to debug memory leak until after new Perl VM exists
            * UTF8
                * Perl::Critic crashes on several CPAN distributions with files containing special characters
                    * Can't parse code: Encountered unexpected character '226'
                    * https://fastapi.metacpan.org/source/PEVANS/Syntax-Operator-In-0.09/t/02elem-numbery.t
                * Existing PPI and/or Perl::Critic issue(s)
                    * https://github.com/Perl-Critic/PPI/issues/22
                    * https://github.com/Perl-Critic/Perl-Critic/issues/562
            * Scroll API
                * Perl code showing the scroll API working
                    * https://github.com/metacpan/metacpan-examples/blob/main/scripts/author/1b-scroll-all-authors-es.pl
                * Ruby code showing the scroll API working
                    * https://github.com/librariesio/libraries.io/pull/2817/files
            * Perl::Critic
                * Policy::Module::RequireEndWithOne
                    * NEED INVESTIGATE POSSIBLE BUG, false violation on Build.PL files which are not actually Perl modules?
        * Current status
            * Have draft LLM.md file containing target LLM training template
            * Organize into granularity of less than 4K tokens per collection of code + docs
                * Distribution
                * Files
                * Packages / Classes
                * Subroutines
            * Currently working on phase 5 to populate LLM.md template
            * Refactored file I/O to includes fork-safe error checking

    * AI Perl Hackathon
        * Very productive Hackathon last night 5/15, we found & fixed the Number-ZipCode-JP memory leak, thanks to everyone who participated!
        * Every 6 weeks alternate between virtual only and hybrid event
        * Next next event 7/3/24, SPECIAL DATE, 7:00-9:00pm Central time zone, theme is PerlGPT or TensorFlow
            * SPECIAL IN-PERSON & VIRTUAL HYBRID EVENT
            * Diogenes Hackerspace, 13800 Dragline Dr, Austin, TX
        * Next event 8/7/24, 7:00-9:00pm Central time zone, theme is PerlGPT or TensorFlow
            * Virtual only

    * Navi AI & Robot  [ NO CHANGE 20240516 ]
        * John & Will got initial refactoring Camel (Catalyst MVC) running, need refactor webcam code w/ Zaki
        * Need Robert Grimes to help get Navi Dockers running locally for John, so he can continue Camel upgrades
        * Navi single-arm robot prototype currently being tested at Diogenes Hackerspace
        * Potential business interest from Hackerspace, trying to find early adoption customers in Austin, will provide more details as available
        * STILL TRYING TO GET NEW CATALYST PROGRAMMER
 
    * Cowl API  [ NO CHANGE 20240516 ]
        * Currently under development, component of Navi's world map
        * RDF::Cowl v1.0.0 released for Valentine's Day!
            * https://metacpan.org/dist/RDF-Cowl
            * https://www.facebook.com/machinelearningperl/posts/pfbid02HNfoDG2E75yszSCVR2E8WHKDbqprknv2afSkuPgeLKKPnL3k91PbXVyNgVRCHNGPl
        * Next steps
            * Alien::Robot
                * http://robot.obolibrary.org/
            * iPerl notebook for gene ontology using Cowl, and eventually all following components
            * Attean / SPARQL (has in-memory triple store)
            * SQLite or other specific triple-store software (to provide on-disk triple store)
                * https://github.com/INCATools/semantic-sql
            * CWPK & KB
            * Docker
            * Navi integration
        * NEED INTEGRATE INTO NAVI AFTER CATALYST REFACTOR
        * NEED MAYBE WORK WITH Adam Russell after Science Track papers are done

    * OpenAI::API  [ NO CHANGE 20240516 ]
        * Need completely-compatible OpenAI client, collaborating with Ryan and John
        * Ryan Voots is developing OpenAI-compatible API using low-level Perl libraries on top of Python LLM loading/inference
        * Ryan is working on SSE already, we will let him show us his SSE and tell him if we like it
        * When Ryan has something ready to test, John can test it and get back with results
        * NEED UPDATE FROM RYAN

    * LangChain  [ NO CHANGE 20240516 ]
        * John wants to start porting LangChain to Perl
        * We will use our prototype Python-to-Perl translator to get started, then invite others to join the project
        * Repositories
            * https://gitlab.com/aiperl/langchain
            * https://gitlab.com/aiperl/langchain-perl

    * Ollama  [ NO CHANGE 20240516 ]
        * From AI Perl Committee group on Facebook
        * John wants to start porting Ollama.ai to Perl
        * Ollama is basically a Golang server that exposes a JSON API that bridges lots of different LLMs and makes local dev easy
        * Ollama client is pretty straightforward to write
        * Ollama may be able to replace Oobabooga, but we would be relacing one non-Perl software with another non-Perl software?
        * Need to discuss in more depth and look at actual code etc

    * BigCode
        * LLM training procedures & datasets for many programming languages
        * They take all their code from GitHub w/out any curating
        * Will says this is a garbage training procedure
        * NEED REPLY FROM BIGCODE
            * Working w/ Dr. Adam Russell to contact BigCode
            * What is the training data format?
            * Will they give us free GPU training time?

    * Code Llama Group (Meta?)  [ NO CHANGE 20240516 ]
        * NEED JOHN TO FIND OUT, same as BigCode above

    * Competing with Python  [ NO CHANGE 20240516 ]
        * Need ported to Perl:
            * OpenAI
            * LangChain
            * Autogen
            * Oobabooga
            * LlamaIndex
            * Ollama
            * BigCode

    * TPC Science Track Talks  [ NO CHANGE 20240516 ]
        * GenProRegEx (placeholder name, NEED REPLACE)
            * Dr. Adam Russell's current 2024 paper
            * Uses genetic programming to learn a regular expression which will extract text entities based on examples
                * Give it examples of phone numbers, it will generate regular expressions to match the phone numbers in training set
        * PerlGPT paper
            * Will & John will partner on PerlGPT presentation
            * John will ask Vincent to be a co-author as well

    * Genetic Programming  [ NO CHANGE 20240516 ]
        * Adam Russell working on this right now
        * NEED MORE INFO & DISCUSSION

    * RoboPerl Nonprofit  [ NO CHANGE 20240516 ]
        * Deep discussion about TPF, PSC, RoboPerl, etc.
        * NEED CREATE BUSINESS PLAN FOR NONPROFIT

* New Business

    * TPF Prospectus Document
        * TPF requested Will to provide an AI Perl write-up for their prospectus, did provide along with website link
        * TPF admin assistant Amber Krawczyk accepted and included as part of 2024 prospectus

    * TPC Normal Track Talks
        * Will got approved for a talk "The State of the Onion AI"

* Closing, 2:29pm
