# AI Perl Committee
# Meeting Agenda & Minutes

## December 28th, 2023
## Eleventh Official Meeting

* Opening, 2:03pm

* Attendance & Introductions (already done in preceding meeting)
    * Will Braswell (computer scientist, creator of Navi AI)
    * John Napiorkowski (maintainer of Catalyst)

* Announcements
    * Chairman John Napiorkowski requests 1st Vice Chairman Will Braswell to preside
    * Meet every 2 weeks on Thursday 2:00-3:00pm Central time zone
        * Immediately following Science Perl Committee and Perl::Types Committee meetings

* Previous Meeting's Minutes
    * 12/14/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by John Napiorkowski to be accepted as read, voted unanimously

* Old Business

    * TensorFlow
        * No update
        * RoboPerl nonprofit grant, approved for these 3 TensorFlow projects:
            * Object Detection
                * Need to work w/ Zaki to get Docker integrated into Navi AI
            * Speech Recognition
                * Started with Zaki at last AI Perl Hackathon, see 11/30/23 meeting notes for details & next steps
                    * https://gitlab.com/aiperl/ai-perl-committee/-/blob/main/meetings/20231130_meeting_agenda_and_minutes.md
                * Need to continue working on this with Zaki, possibly find someone to help if they exist?
            * Optical Character Recognition
                * Not started yet

    * PerlGPT
        * Phase 1, TPF grant re-proposal, approved
            * Robert Grimes will provide initial training process
            * Ryan Voots wants to help build the initial PerlGPT training data set, specifically the CPAN part first
            * John Napiorkowski and Will Braswell will implement the technical process

    * AI Perl Hackathon
        * Every 6 weeks
        * Next event 1/10/24, 7:00-9:00pm Central time zone, theme is TensorFlow Speech Recognition

    * Navi AI & robot
        * John & Will got initial refactoring Camel (Catalyst MVC) running, need delete old pre-refactor code
        * Need Robert Grimes to help get Navi Dockers running locally for John, so he can continue Camel upgrades
        * Navi single-arm robot prototype currently being tested at Diogenes Hackerspace, goal is Valentines Day upgrade
 
    * New unreleased AI & science software component
        * Currently under development
        * Component of Navi's world map
        * Alien interface now working
        * Majority of C data types & functions are now working in Perl
        * Next CPAN release soon, possibly as holiday release

    * OpenAI::API
        * Ryan Voots is developing OpenAI-compatible API using low-level Perl libraries on top of Python LLM loading/inference
        * John is developing Server-Side Events (SSE) Support
        * Need to meet with Ryan and John to compare current development and determine best strategy forward
        * Need completely-compatible OpenAI client, can collaborate with Ryan and John

* New Business

    * Competing with Python
        * Need OpenAI    ported to Perl
        * Need Autogen   ported to Perl
        * Need Langchain ported to Perl
        * Need Oobabooga ported to Perl

* Closing, 2:32pm
