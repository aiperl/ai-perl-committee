# AI Perl Committee
# Meeting Agenda & Minutes

## January 11th, 2024
## Twelfth Official Meeting

* Opening, 2:24pm

* Attendance & Introductions (already done in preceding meeting)
    * Brett Estrade (high-performance computing)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Joshua Day (industrial tooling)
    * John Napiorkowski (maintainer of Catalyst)

* Announcements
    * Chairman John Napiorkowski requests 1st Vice Chairman Will Braswell to preside
    * Meet every 2 weeks on Thursday 2:00-3:00pm Central time zone
        * Immediately following Science Perl Committee and Perl::Types Committee meetings

* Previous Meeting's Minutes
    * 12/28/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by John Napiorkowski to be accepted as read, voted unanimously

* Old Business

    * TensorFlow
        * On hold until other Zaki project is complete

        * RoboPerl nonprofit grant, approved for these 3 TensorFlow projects:
            * Object Detection
                * Need to work w/ Zaki to get Docker integrated into Navi AI
            * Speech Recognition
                * Started with Zaki at last AI Perl Hackathon, see 11/30/23 meeting notes for details & next steps
                    * https://gitlab.com/aiperl/ai-perl-committee/-/blob/main/meetings/20231130_meeting_agenda_and_minutes.md
                * Need to continue working on this with Zaki, possibly find someone to help if they exist?
            * Optical Character Recognition
                * Not started yet

    * PerlGPT
        * Phase 1, TPF grant re-proposal, approved
        * We will probably use the MetaCPAN API directly
        * Created new repository
            * git@gitlab.com:aiperl/perlgpt.git

    * AI Perl Hackathon
        * 3rd Hackathon held last night
            * Theme was ChatGPU Navi
            * John & Will did pair programming for Navi mind refactoring
            * Joined by a number of other Perl programmers including Joshua Day, Robbie Hatley, and Jay Battikha
            * Created new database tables to hold webcam images and TensorFlow image classification data
        * Every 6 weeks
        * Next event 2/21/24, 7:00-9:00pm Central time zone, theme is TensorFlow Speech Recognition

    * Navi AI & robot
        * John & Will got initial refactoring Camel (Catalyst MVC) running, need refactor webcam code w/ Zaki
        * Need Robert Grimes to help get Navi Dockers running locally for John, so he can continue Camel upgrades
        * Navi single-arm robot prototype currently being tested at Diogenes Hackerspace
 
    * New unreleased AI & science software component
        * Currently under development, component of Navi's world map
        * Remaining before CPAN release are tests, documentation, and Gitlab CI
        * CPAN release hopefully this week

    * OpenAI::API
        * Need completely-compatible OpenAI client, collaborating with Ryan and John
        * Ryan Voots is developing OpenAI-compatible API using low-level Perl libraries on top of Python LLM loading/inference
        * Ryan is working on SSE already, we will let him show us his SSE and tell him if we like it
        * When Ryan has something ready to test, John can test it and get back with results

    * Competing with Python
        * Need ported to Perl:
            * OpenAI
            * LangChain
            * Autogen
            * Oobabooga
            * LlamaIndex

* New Business

    * LangChain

        * John wants to start porting LangChain immediately
        * We will use our prototype Python-to-Perl translator to get started, then invite others to join the project
        * Created new repositories
            * https://gitlab.com/aiperl/langchain
            * https://gitlab.com/aiperl/langchain-perl

* Closing, 2:53pm
