# AI Perl Committee
# Meeting Agenda & Minutes

## March 7th, 2024
## Sixteenth Official Meeting

* Opening, 2:10pm

* Attendance & Introductions (already done in preceding meeting)
    * Will Braswell (computer scientist, creator of Navi AI)
    * John Napiorkowski (maintainer of Catalyst)
    * Jakub Pawlowski (wants to integrate Perl AI with .NET & Mono)
    * Mickey Macten
    * Joshua C Oliva

* Announcements
    * Chairman John Napiorkowski requests 1st Vice Chairman Will Braswell to preside
    * Meet every 2 weeks on Thursday 2:00-3:00pm Central time zone
        * Immediately following Science Perl Committee and Perl::Types Committee meetings

* Previous Meeting's Minutes
    * 2/8/24 meeting minutes
    * 2/22/24 meeting minutes
    * BOTH READ by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by John Napiorkowski FOR BOTH to be accepted as read, voted unanimously

* Old Business

    * TensorFlow
        * RoboPerl nonprofit grant, approved for these 3 TensorFlow projects:
            * Object Detection
                * Need to work w/ Zaki to get Docker integrated into Navi AI
            * Speech Recognition
                * https://gitlab.com/zmughal/notebook-perl-libtensorflow-speech-recognition
                * Continued with Zaki at last AI Perl Hackathon
                * Zaki may do more coding via live stream (Twitch?) ???
            * Optical Character Recognition
                * Not started yet

    * PerlGPT
        * Phase 1, TPF grant re-proposal, approved
            * Sent first official grant report to Saif at TPF, waiting for his reply
        * Repository
            * git@gitlab.com:aiperl/perlgpt.git
        * We are using the MetaCPAN API directly
            * Search through all CPAN for good training distributions
            * Must be recent, last 3 - 5 years to start, add older if needed
            * Must pass CPAN Testers with high pass rates, 90% or higher
            * Must pass CPANTS Kwalitee with high score, 25 out of 30 or higher
            * Automatically annotate Perl::Critic violations with high severity, 4 out of 5 or higher
            * Organize into granularity of less than 4K tokens per collection of code + docs
                * Distribution
                * Files
                * Packages / Classes
                * Subroutines
            * Somehow give preference to well-written code containing descriptive comments?
            * Current issues
                * UTF8
                    * Perl::Critic crashes on several CPAN distributions with files containing special characters
                        * Can't parse code: Encountered unexpected character '226'
                        * https://fastapi.metacpan.org/source/PEVANS/Syntax-Operator-In-0.09/t/02elem-numbery.t
                    * Existing PPI and/or Perl::Critic issue(s)
                        * https://github.com/Perl-Critic/PPI/issues/22
                        * https://github.com/Perl-Critic/Perl-Critic/issues/562
                * Scroll API
                    * Perl code showing the scroll API working
                        * https://github.com/metacpan/metacpan-examples/blob/main/scripts/author/1b-scroll-all-authors-es.pl
                    * Ruby code showing the scroll API working
                        * https://github.com/librariesio/libraries.io/pull/2817/files

    * AI Perl Hackathon
        * Every 6 weeks
        * Next event 4/3/24, 7:00-9:00pm Central time zone, theme is TensorFlow Speech Recognition
            * SPECIAL IN-PERSON & VIRTUAL HYBRID EVENT
            * Diogenes Hackerspace, 13800 Dragline Dr, Austin, TX

    * Navi AI & robot
        * John & Will got initial refactoring Camel (Catalyst MVC) running, need refactor webcam code w/ Zaki
        * Need Robert Grimes to help get Navi Dockers running locally for John, so he can continue Camel upgrades
        * Navi single-arm robot prototype currently being tested at Diogenes Hackerspace
 
    * Cowl API
        * Currently under development, component of Navi's world map
        * RDF::Cowl v1.0.0 released for Valentine's Day!
            * https://metacpan.org/dist/RDF-Cowl
            * https://www.facebook.com/machinelearningperl/posts/pfbid02HNfoDG2E75yszSCVR2E8WHKDbqprknv2afSkuPgeLKKPnL3k91PbXVyNgVRCHNGPl
        * Next steps
            * Alien::Robot
                * http://robot.obolibrary.org/
            * iPerl notebook for gene ontology using Cowl, and eventually all following components
            * Attean / SPARQL (has in-memory triple store)
            * SQLite or other specific triple-store software (to provide on-disk triple store)
                * https://github.com/INCATools/semantic-sql
            * CWPK & KB
            * Docker
            * Navi integration

    * OpenAI::API
        * Need completely-compatible OpenAI client, collaborating with Ryan and John
        * Ryan Voots is developing OpenAI-compatible API using low-level Perl libraries on top of Python LLM loading/inference
        * Ryan is working on SSE already, we will let him show us his SSE and tell him if we like it
        * When Ryan has something ready to test, John can test it and get back with results

    * LangChain
        * John wants to start porting LangChain to Perl
        * We will use our prototype Python-to-Perl translator to get started, then invite others to join the project
        * Repositories
            * https://gitlab.com/aiperl/langchain
            * https://gitlab.com/aiperl/langchain-perl

    * Ollama
        * From AI Perl Committee group on Facebook
        * John wants to start porting Ollama.ai to Perl
        * Ollama is basically a Golang server that exposes a JSON API that bridges lots of different LLMs and makes local dev easy
        * Ollama client is pretty straightforward to write
        * Ollama may be able to replace Oobabooga, but we would be relacing one non-Perl software with another non-Perl software?
        * Need to discuss in more depth and look at actual code etc

    * Competing with Python
        * Need ported to Perl:
            * OpenAI
            * LangChain
            * Autogen
            * Oobabooga
            * LlamaIndex
            * Ollama

* New Business

    * Website
        * perlcommunity.org/ai
        * Now published, need to officially release
        * Reviewed with committee
        * Will Braswell makes a motion to adopt as committee's official website, unanimous in favor

    * Minimum Quorum Requirements
        * Will Braswell makes a motion to adopt 5 as the minimum number of committee members required for a voting quorum
        * Seconded by John Napiorkowski
        * Vote results are 100% unanimous in favor of 5 minimum committee members required for an official voting quorum from now on

* Closing, 2:38pm
