# AI Perl Committee
# Meeting Agenda & Minutes

## November 16th, 2023
## Eighth Official Meeting

* Opening, 2:08pm

* Attendance & Introductions (already done in preceding meeting)
    * John Napiorkowski (involved in Perl since 90's, helping Perl make people happy and making Perl competitive)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Robert Grimes (Perl programmer, member of Navi AI development team)

* Announcements
    * Chairman John Napiorkowski requests 1st Vice Chairman Will Braswell to preside
    * Meet every 2 weeks on Thursday 2:00-3:00pm Central time zone
        * Immediately following Science Perl Committee and Perl::Types Committee meetings

* Previous Meeting's Minutes
    * 11/2/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell, seconded by John Napiorkowski to be accepted as read, voted unanimously

* Old Business

    * TensorFlow
        * Phase 2, TPF grant proposal
            * Still waiting for response from TPF president, promised 12 weeks ago, presumably they are purposefully misleading or delaying
        * Need to work with Zaki to create new Docker for Object Detection neural network
        * New Perl Labs nonprofit has received a donation for the following 3 TensorFlow projects
            * Object Detection
            * Speech Recognition
            * Optical Character Recognition
        * We have somebody who may want to work on TensorFlow to take some pressure off Zaki

    * PerlGPT
        * Phase 1, TPF grant re-proposal
            * https://news.perlfoundation.org/post/perlgptphase1
            * Proposal published 9/20/23, 2-week public comment phase now complete, TPF Grant Committee was supposed to have already voted
            * Saif asked for a link to the existing Code Llama repository, we sent it to them and are waiting for feedback

    * AI Perl Hackathon
        * Every 6 weeks
        * Next event 11/29/23, SPECIAL HOLIDAY TIME 8:30-10:00pm Central time zone, theme is Machine Vision P2?
            * Date & time are currently subject to change due to holidays etc.

    * Navi AI
        * John & Will are refactoring Camel (Catalyst MVC) to break out Navi consciousness run loop
        * Navi robot prototype currently being designed, will review specifications at next meeting
        
    * Financial support
        * 3 tiers of Perl organizations
            * Commercial via ChatGPU & APTech
            * Mutual aid society via Perl Professional Association
            * Non-profit via Perl Labs
        * ChatGPU will pursue Kickstarter for Navi robot

    * New unreleased AI & science software component
        * Currently under development
        * Component of Navi's world map
        * Alien interface running now, moving on to the main API soon

    * OpenAI::API
        * OpenAI::API on CPAN is currently behind, does not support streaming
        * John will contact original author Nelson Ferraz

* New Business
    * none

* Closing, 2:47pm
