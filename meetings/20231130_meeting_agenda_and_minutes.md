# AI Perl Committee
# Meeting Agenda & Minutes

## November 30th, 2023
## Ninth Official Meeting

* Opening, 2:39pm

* Attendance & Introductions (already done in preceding meeting)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Zakariyya Mughal (BioPerl, PDL, previously biomedical imaging, currently computational toxicology)

* Announcements
    * Chairman John Napiorkowski requests 1st Vice Chairman Will Braswell to preside
    * Meet every 2 weeks on Thursday 2:00-3:00pm Central time zone
        * Immediately following Science Perl Committee and Perl::Types Committee meetings

* Previous Meeting's Minutes
    * 11/16/23 meeting minutes
    * NOT read by Acting Secretary Will Braswell, tabled

* Old Business

    * TensorFlow
        * Phase 2, TPF grant proposal
            * Still waiting for response from TPF president, promised 14 weeks ago, presumably they are purposefully misleading or delaying
        * RoboPerl nonprofit grant, approved for these 3 TensorFlow projects:
            * Object Detection
                * Almost done
                * Need to work w/ Zaki to get Docker integrated into Navi AI
            * Speech Recognition
                * Started with Zaki at AI Perl Hackathon last night, got TensorFlowASR demo running using Conformer back-end
                    * https://github.com/TensorSpeech/TensorFlowASR
                * Created new script to build and install TensorFlowASR demo:
                    * https://gist.github.com/zmughal/8c34000c35a2dd71f050b7f508672053
                * Neural network weights currently saved in HDF5 format, can load and be saved out again
                * Need to convert HDF5 format into Saved Model format (directory containing files)
            * Optical Character Recognition
                * Not started yet

    * PerlGPT
        * Phase 1, TPF grant re-proposal
            * https://news.perlfoundation.org/post/perlgptphase1
            * Proposal approved 11/21/23, congratulations to everyone involved
            * Work will begin soon

    * AI Perl Hackathon
        * Second hackathon last night
            * Great work, got started on speech recognition TensorFlow, see details above
        * Every 6 weeks
        * Next event 1/10/24, 7:00-9:00pm Central time zone, theme is TensorFlow Speech Recognition or Optical Character Recognition

    * Navi AI
        * John & Will are refactoring Camel (Catalyst MVC) to break out Navi consciousness run loop
        * Navi robot prototype currently being designed, parts have been ordered
 
    * New unreleased AI & science software component
        * Currently under development
        * Component of Navi's world map
        * Alien interface running, some parts of API working

    * OpenAI::API
        * OpenAI::API on CPAN is currently behind, does not support streaming
        * John will contact original author Nelson Ferraz

* New Business
    * none

* Closing, 2:53pm
