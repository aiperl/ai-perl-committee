# AI Perl Committee
# Meeting Agenda & Minutes

## January 25th, 2024
## Thirteenth Official Meeting

* Opening, 2:16pm

* Attendance & Introductions (already done in preceding meeting)
    * Will Braswell (computer scientist, creator of Navi AI)
    * John Napiorkowski (maintainer of Catalyst)
    * Jakub Pawlowski (wants to integrate Perl AI with .NET & Mono)

* Announcements
    * Chairman John Napiorkowski requests 1st Vice Chairman Will Braswell to preside
    * Meet every 2 weeks on Thursday 2:00-3:00pm Central time zone
        * Immediately following Science Perl Committee and Perl::Types Committee meetings

* Previous Meeting's Minutes
    * 1/11/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by John Napiorkowski to be accepted as read, voted unanimously

* Old Business

    * TensorFlow
        * On hold until other Zaki project is complete

        * RoboPerl nonprofit grant, approved for these 3 TensorFlow projects:
            * Object Detection
                * Need to work w/ Zaki to get Docker integrated into Navi AI
            * Speech Recognition
                * Started with Zaki at last AI Perl Hackathon, see 11/30/23 meeting notes for details & next steps
                    * https://gitlab.com/aiperl/ai-perl-committee/-/blob/main/meetings/20231130_meeting_agenda_and_minutes.md
                * Need to continue working on this with Zaki, possibly find someone to help if they exist?
            * Optical Character Recognition
                * Not started yet

    * PerlGPT
        * Phase 1, TPF grant re-proposal, approved
        * We will use the MetaCPAN API directly
            * Last 3 years of code
            * CPAN Testers with high pass rates, 90% or higher
            * Code Kwalitee is not useful?
            * Well-written individual subroutines containing descriptive comments and < 4K tokens?
        * Repository
            * git@gitlab.com:aiperl/perlgpt.git
        * Need to write initial report and send to TPF ASAP

    * AI Perl Hackathon
        * Every 6 weeks
        * Next event 2/21/24, 7:00-9:00pm Central time zone, theme is PerlGPT and/or TensorFlow Speech Recognition

    * Navi AI & robot
        * John & Will got initial refactoring Camel (Catalyst MVC) running, need refactor webcam code w/ Zaki
        * Need Robert Grimes to help get Navi Dockers running locally for John, so he can continue Camel upgrades
        * Navi single-arm robot prototype currently being tested at Diogenes Hackerspace
 
    * New unreleased AI & science software component
        * Currently under development, component of Navi's world map
        * Remaining before CPAN release are tests, documentation, and Gitlab CI
        * Currently have partial tests running!
        * CPAN release hopefully next week

    * OpenAI::API
        * Need completely-compatible OpenAI client, collaborating with Ryan and John
        * Ryan Voots is developing OpenAI-compatible API using low-level Perl libraries on top of Python LLM loading/inference
        * Ryan is working on SSE already, we will let him show us his SSE and tell him if we like it
        * When Ryan has something ready to test, John can test it and get back with results

    * LangChain
        * John wants to start porting LangChain to Perl
        * We will use our prototype Python-to-Perl translator to get started, then invite others to join the project
        * Repositories
            * https://gitlab.com/aiperl/langchain
            * https://gitlab.com/aiperl/langchain-perl

    * Competing with Python
        * Need ported to Perl:
            * OpenAI
            * LangChain
            * Autogen
            * Oobabooga
            * LlamaIndex
            * Ollama

* New Business

    * Ollama
        * From AI Perl Committee group on Facebook
        * John wants to start porting Ollama.ai to Perl
        * Ollama is basically a Golang server that exposes a JSON API that bridges lots of different LLMs and makes local dev easy
        * Ollama client is pretty straightforward to write
        * Ollama may be able to replace Oobabooga, but we would be relacing one non-Perl software with another non-Perl software?
        * Need to discuss in more depth and look at actual code etc

* Closing, 2:33pm
