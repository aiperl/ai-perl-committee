# AI Perl Committee
# Meeting Agenda & Minutes

## December 14th, 2023
## Tenth Official Meeting

* Opening, 2:03pm

* Attendance & Introductions (already done in preceding meeting)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Joshua Day (industrial tooling)
    * John Napiorkowski (maintainer of Catalyst)

* Announcements
    * Chairman John Napiorkowski requests 1st Vice Chairman Will Braswell to preside
    * Meet every 2 weeks on Thursday 2:00-3:00pm Central time zone
        * Immediately following Science Perl Committee and Perl::Types Committee meetings

* Previous Meeting's Minutes
    * 11/16/23 meeting minutes
    * 11/30/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by John Napiorkowski to be accepted as read, voted unanimously

* Old Business

    * TensorFlow
        * RoboPerl nonprofit grant, approved for these 3 TensorFlow projects:
            * Object Detection
                * Need to work w/ Zaki to get Docker integrated into Navi AI
            * Speech Recognition
                * Started with Zaki at last AI Perl Hackathon, see 11/30/23 meeting notes for details & next steps
                    * https://gitlab.com/aiperl/ai-perl-committee/-/blob/main/meetings/20231130_meeting_agenda_and_minutes.md
                * Need to continue working on this with Zaki, possibly find someone to help if they exist?
            * Optical Character Recognition
                * Not started yet

    * PerlGPT
        * Phase 1, TPF grant re-proposal, approved
            * Robert Grimes will provide initial technical process
            * John Napiorkowski and Will Braswell will implement the technical process
            * We are looking to see if anyone else may want to join this project

    * AI Perl Hackathon
        * Every 6 weeks
        * Next event 1/10/24, 7:00-9:00pm Central time zone, theme is TensorFlow Speech Recognition

    * Navi AI
        * John & Will got initial refactoring Camel (Catalyst MVC) running, able to break out Navi consciousness run loop with 1 subroutine
        * Need Robert Grimes to help get Navi Dockers running locally for John, so he can continue Camel upgrades
        * Navi robot prototype currently being built
 
    * New unreleased AI & science software component
        * Currently under development
        * Component of Navi's world map
        * Alien interface now working
        * Majority of C data types are now working in Perl
        * Next are C functions

    * OpenAI::API
        * OpenAI::API on CPAN is currently behind, does not support streaming
        * John contacted original author Nelson Ferraz on IRC, no reply yet; needs to do following SSE work before contacting him again
        * Server-Side Events (SSE) Support
            * Need to support SSE because OpenAI's streaming requires it, the streams are by tokens
            * John wants to add better support for SSE into the 2 most common loop-aware clients, Mojo HTTP client & IO::Async HTTP client
            * Currently working with hacked version of Mojo client, will try to get Mojo group to make it a base feature
            * Lots of other APIs are using server-side events for streaming, not just OpenAI

* New Business
    * none

* Closing, 2:42pm
