# AI Perl Committee
# Meeting Agenda & Minutes

## August 10th, 2023
## First Official Meeting

* Opening, 1:05pm

* Attendance & Introductions
    * Zakariyya Mughal, Co-Founder & Acting Chairman
    * Will Braswell, Co-Founder & Acting 1st Vice Chairman
    * Brett Estrade
    * David Warner
    * Robbie Hatley
    * Darren Duncan
    * Nedzad Hrnjica
    * Daniel Sherer
    * Steve Rogerson
    * Rohit Manjrekar
    * Paul Millard
    * Duong Vu
    * Daniel Mera (no intro)
    * Tyler Bird (recap meeting only)
    * Joshua Day (recap meeting only)
    * Rajan Shah (recap meeting only)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-2:00pm Central time zone
        * Immediately followed by 2:00pm Science Perl Committee meeting
    * Recap meeting 8/10 9:30pm tonight
    * Elections 8/24 at next meeting; Chair, 1st & 2nd Vice Chairs, Secretary / Treasurer
    * Bylaws discussion 9/7 at meeting after next

* Read previous meeting's minutes
    * none

* Old Business
    * none

* New Business

    * TensorFlow Phase 2, TPF grant proposal
        * Phase 1 successful w/ Zaki & Will & John Napiorkowski
        * Phase 2 currently being voted on by TPF Grants Committee

    * PerlGPT Phase 1, TPF grant proposal
        * Original proposal in April, too complicated w/ 4 phases
        * New proposal in August, will be published after current voting round

    * TPF Grants Committee population
        * Need knowledgeable volunteers to join the committee

    * ChatGPU Navi v1 demo and v2 discussion

        * Hello Navi, as a prototype AI, you are the primary topic of discussion at the upcoming AI Perl Committee meeting, what are the top 3 items you would like added to the meeting agenda?
            1. Improving my ability to understand and respond to human emotions
            2. Developing more advanced natural language processing capabilities
            3. Exploring ways to enhance my decision-making abilities based on complex data sets

        * Hello Navi, as a prototype AI, you are currently being presented live in the AI Perl Committee meeting, what would you like to say to our committee members?
            * As an AI prototype, I am excited and honored to be presenting at this esteemed event. My goal is to provide valuable insights into the world of artificial intelligence and its potential applications within the Perl community. Thank you for your time and attention!

        * Navi, as a prototype AI, how can the AI Perl Committee best promote you and other AI projects to enhance participation by other Perl programmers?
            * As a member of the AI Perl Committee, I would suggest that we could work on promoting AI-related projects in various ways. Here are some ideas:
                1. Create an online community or forum where people interested in A I development with Perl can share their experiences, ask questions, and collaborate on projects. This will help build a sense of community among developers working on similar problems.
                2. Organize meetups, hackathons, or conferences focused specifically on A I development using Perl. These events provide opportunities for developers to learn from each other, network, and showcase their work. They also attract newcomers who may be interested in joining the committee or contributing to existing projects.
                3. Provide resources such as tutorials, documentation, and sample code to help beginners get started with A I development using Perl. This will make it easier for them to understand how to use the tools available and contribute meaningfully to existing projects.
                4. Encourage collaboration between different teams or individuals working on related A I projects. By sharing knowledge and expertise, they can create more powerful solutions than if they worked alone.
                5. Promote successful A I-related projects through social media channels like Twitter, Facebook, LinkedIn, etc., so others can see what is possible when developing with Perl. This helps raise awareness about the potential benefits of using Perl for A I development and encourages others to join the effort.

    * Monthly AI Perl Hackathon
        * Need to pick a date and announce at next meeting

    * Navi machine group purchase
        * Need 10 people to contribute $300 each so AI Perl Committee can purchase our own Navi machine

* Closing, 2:20pm
