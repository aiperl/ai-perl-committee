# AI Perl Committee
# Meeting Agenda & Minutes

## August 8th, 2024
## Twenty-Sixth Official Meeting

* Opening, 2:20pm

* Attendance & Introductions (already done in preceding meeting)
    * John Napiorkowski (maintainer of Catalyst)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Tom Bodine (started Perl at UT in 1994, GPS sattelite mapping applications using Perl; server penetration monitoring at Caribbean Online; memory tests at AMD)
    * Robbie Hatley (EE & computer programming in Perl)
    * Steven McDougall (1980's medical ultrasound technology, 1990's genomic sequencing, using Perl to work on scientific problems)

* Announcements
    * Chairman John Napiorkowski requests 1st Vice Chairman Will Braswell to preside
    * Meet every 2 weeks on Thursday 2:00-3:00pm Central time zone
        * Immediately following Science Perl Committee and Perl::Types Committee meetings

* Previous Meeting's Minutes
    * 7/25/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by John Napiorkowski to be accepted as corrected, voted unanimously
    * NEED SPECIAL BYLAW to allow meeting minutes approval with minimum of one officer only, not full voting quorum

* Old Business

    * Website  [ NO CHANGE 20240808 ]
        * perlcommunity.org/ai
        * perlcommunity.org/awards
            * NEED FINISH

    * TensorFlow
        * RoboPerl nonprofit grant, approved for these 3 TensorFlow projects:
            * Object Detection
                * Need to work on getting Docker integrated into Navi AI
            * Speech Recognition
                * https://gitlab.com/zmughal/notebook-perl-libtensorflow-speech-recognition
                * Need new programmer
            * Optical Character Recognition
                * Not started yet
        * STILL TRYING TO WORK WITH POTENTIAL NEW PROGRAMMER
        * John contacted Zaki
            * NEED REPLY FROM ZAKI

    * PerlGPT
        * Phase 1, TPF grant re-proposal, initially approved, WORKING TO RE-APPLY
            * $1,100 Phase
                * Found e-mail from TPF on 11/21/23 listing $1.1K & $7.7K
                * John asked Saif for $1.1K payment ASAP, today
                * Saif said yes he will pay the $1.1K, says he is still working on it
            * $7,700 Phase
                * John asked Saif how to get $7.7K restarted, last week or so
                * STILL NEED REPLY FROM SAIF
                * NEED SEND PERLGPT PAPER
                * NEED TO WRITE NEW PROPOSAL
        * Repositories
            * https://gitlab.com/aiperl/perlgpt
            * https://gitlab.com/aiperl/perlgpt-working-tmp
        * We are using the MetaCPAN API directly
            * Search through all CPAN for good training distributions
            * Must be recent, last 3 - 5 years to start, add older if needed
            * Must pass CPAN Testers with high pass rates, 90% or higher
            * Must pass CPANTS Kwalitee with high score, 25 out of 30 or higher
            * Automatically annotate Perl::Critic violations with high severity, 4 out of 5 or higher
            * Somehow give preference to well-written code containing descriptive comments?
        * Current issues
            * Even more memory leaks
                * 'Number-ZipCode-JP'  => 'causes memory leak, presumably due to long Kanji and/or regex strings',
                * 'Jacode'             => 'causes memory leak, presumably due to long Kanji and/or regex strings',
                * 'Jacode4e'           => 'MAYBE causes memory leak, presumably due to long Kanji and/or regex strings',
                * 'Jacode4e-RoundTrip' => 'MAYBE causes memory leak, presumably due to long Kanji and/or regex strings',
                * 'Lingua-EN-Opinion'  => 'causes memory leak, presumably due to long wordlist data structures in lib/Lingua/EN/Opinion/Emotion.pm',
            * High memory usage distributions AKA memory hogs
                * 'Chart-Plotly'       => 'MAYBE runs long & hogs memory, presumably due to long number lists in examples/traces/volume.pl etc',
                * 'HiPi'               => 'runs long & hogs memory, presumably due to long fonts in lib/HiPi/Graphics/BitmapFont/MonoEPD102.pm etc',
                * 'Date-Manip'         => 'runs long & hogs memory, presumably due to long international characters in lib/Date/Manip/DM5.pm etc',
                * 'Games-Axmud'        => 'runs long & hogs memory, presumably due to long file lib/Games/Axmud/Cmd.pm etc',
                * 'Paws'               => 'runs long & hogs memory, presumably due to large number of files',
            * MetaCPAN API Filter Bug
                * the 'filter' parameter below has some unknown issues,
                    * `filter => { term => { status => 'latest' }},  # only return latest versions of each distribution`
                * on 20240604 'Author-Daemon-Site-Ptr-Bond' was returned twice in the same API call
                * both Author-Daemon-Site-Ptr-Bond-0.008.tar.gz and Author-Daemon-Site-Ptr-Bond-0.004.tar.gz
                * this caused the accepted + rejected distributions to be 4999 instead of 5000, and possibly other undetected secondary effects
                * More info in my code comments
                    * https://gitlab.com/aiperl/perlgpt/-/commit/3099e3b1ecf3c262bca2fef0c49b8f60d3aac699#b35c7fa05a28c5da3d70bbc98b1229a5a0883861_416_421
                * NEED FILE METACPAN BUG REPORT?
                    * Will says the MetaCPAN API should be able to detect bad or duplicate metadata and filter correctly
                * NEED INFORM AUTHOR OF BAD METADATA?
                    * John says it is caused by bad metadata in one or both of those release files
            * Number-ZipCode-JP
                * Distribution contains Kanji characters and huge Perl regexes
                * Causes memory leak in Perl::Tidy or Perl::Critic or PPI
                * Even Parallel::ForkManager can't contain it!
                * Disabled for now, may not be possible to debug memory leak until after new Perl VM exists
            * UTF8
                * Perl::Critic crashes on several CPAN distributions with files containing special characters
                    * Can't parse code: Encountered unexpected character '226'
                    * https://fastapi.metacpan.org/source/PEVANS/Syntax-Operator-In-0.09/t/02elem-numbery.t
                * Existing PPI and/or Perl::Critic issue(s)
                    * https://github.com/Perl-Critic/PPI/issues/22
                    * https://github.com/Perl-Critic/Perl-Critic/issues/562
            * Scroll API
                * Perl code showing the scroll API working
                    * https://github.com/metacpan/metacpan-examples/blob/main/scripts/author/1b-scroll-all-authors-es.pl
                * Ruby code showing the scroll API working
                    * https://github.com/librariesio/libraries.io/pull/2817/files
            * Perl::Critic
                * Policy::Module::RequireEndWithOne
                    * NEED INVESTIGATE POSSIBLE BUG, false violation on Build.PL files which are not actually Perl modules?
        * Current status
            * Have draft LLM.md file containing target LLM training template
            * Organize into granularity of less than 4K tokens per collection of code + docs
                * Distribution
                * Files
                * Packages / Classes
                * Subroutines
            * Currently working on phase 5 to populate LLM.md template
            * Refactored file I/O to includes fork-safe error checking
            * Made list of all Kwalitee indicators for future fine-tuning of acceptance/rejection criteria
            * PerlGPT MetaCPAN Curator paper mostly completed for Science Perl Journal, reviewed by Dr. Marc Perry
                * Updated the comments 
                * Found new memory leaks, memory hogs, and API filter bug
            * Training
                * Ryan Voots, local LLM server
                    * installed unsloth, NEED TEST
                    * installed HuggingFace AutoTrain, NEED TEST
                * need feedback about Brev.dev website
                * NEED JOHN & RYAN FEEDBACK ABOUT LORA TRAINING VIDEO

    * AI Perl Hackathon
        * Every 6 weeks alternate between virtual only and hybrid event
        * Previous event 8/7/24, 7:00-9:00pm Central time zone, accidental theme was Perl::Types
            * Virtual only
        * Next event 9/18/24, 7:00-9:00pm Central time zone, theme is PerlGPT
            * SPECIAL IN-PERSON & VIRTUAL HYBRID EVENT
            * Diogenes Hackerspace, 13800 Dragline Dr, Austin, TX
            * Meetup Event, please RSVP
                * https://www.meetup.com/austin-perl-mongers/events/
        * Next next event 8/7/24, 7:00-9:00pm Central time zone, theme is PerlGPT or TensorFlow
            * Virtual only

    * Navi AI  [ NO CHANGES 20240808 ]
        * John & Will got initial refactoring Camel (Catalyst MVC) running, need refactor webcam code w/ Zaki
        * Need Robert Grimes to help get Navi Dockers running locally for John, so he can continue Camel upgrades
        * STILL TRYING TO GET NEW CATALYST PROGRAMMER
        * NEED PAIR PROGRAMMING SESSION TO MOVE PAST WEBCAM & DOCKER

    * Navi Robot  [ NO CHANGES 20240808 ]
        * Navi single-arm robot prototype currently being tested at Diogenes Hackerspace
        * Potential business interest from Hackerspace, trying to find early adoption customers in Austin, will provide more details as available
        * Purchased airline approved carry-on 100% aluminum protective case, with pick-and-pluck foam
            * Will have to remove wheels and fold down camera to fit inside maximum allowable size of airline restrictions
        * Purchased bluetooth keyboard & touchpad combo, Logitech K400, allows us to actually utilize Navi robot's built-in ROS
        * STILL TRYING TO DEBUG THE RANDOM BOOT ISSUE
            * 3 beeps at end of boot sequence indicates proper functionality
            * lack of 3 beeps allows tablet app to connect & view cameras, but not control arm or wheels
            * Need purchase powered USB hub to test possible power distribution bug
            * Need to contact manufacturer or other robot developers
            * Using K400 to reboot via ROS GUI usually causes the 3 beeps after 1 reboot
        * NEED PURCHASE LARGER CARRYING CASE, to avoid disassembly
        * NEED CREATE NAVI ROBOT MEETUP
        * NEED CONTACT ALREADY-INTERESTED PARTIES
        * NEED PERLGPT TO WRITE PERL API FOR ROS, will enable Navi AI to control Navi robot
 
    * Cowl API  [ NO CHANGES 20240808 ]
        * Currently under development, component of Navi's world map
        * RDF::Cowl v1.0.0 released for Valentine's Day!
            * https://metacpan.org/dist/RDF-Cowl
            * https://www.facebook.com/machinelearningperl/posts/pfbid02HNfoDG2E75yszSCVR2E8WHKDbqprknv2afSkuPgeLKKPnL3k91PbXVyNgVRCHNGPl
        * Next steps
            * Alien::Robot
                * http://robot.obolibrary.org/
            * iPerl notebook for gene ontology using Cowl, and eventually all following components
            * Attean / SPARQL (has in-memory triple store)
            * SQLite or other specific triple-store software (to provide on-disk triple store)
                * https://github.com/INCATools/semantic-sql
            * CWPK & KB
            * Docker
            * Navi integration
        * NEED INTEGRATE INTO NAVI AFTER CATALYST REFACTOR
        * NEED CONTACT DR. RUSSELL FOR COWL PLANS

    * OpenAI::API
        * Need completely-compatible OpenAI client, collaborating with Ryan and John
        * Ryan Voots is developing OpenAI-compatible API using low-level Perl libraries on top of Python LLM loading/inference
        * Ryan is working on SSE already, we will let him show us his SSE and tell him if we like it
        * When Ryan has something ready to test, John can test it and get back with results
        * Ryan Voots is planning to restart work on the OpenAI-compatible API, possibly for use with AutoTrain and unsloth etc
        * NEED JOHN TO CONTACT NELSON FERRAZ AGAIN

    * LangChain  [ NO CHANGE 20240808 ]
        * John wants to start porting LangChain to Perl
        * We will use our prototype Python-to-Perl translator to get started, then invite others to join the project
        * Repositories
            * https://gitlab.com/aiperl/langchain
            * https://gitlab.com/aiperl/langchain-perl

    * Ollama  [ NO CHANGE 20240808 ]
        * From AI Perl Committee group on Facebook
        * John wants to start porting Ollama.ai to Perl
        * Ollama is basically a Golang server that exposes a JSON API that bridges lots of different LLMs and makes local dev easy
        * Ollama client is pretty straightforward to write
        * Ollama may be able to replace Oobabooga, but we would be relacing one non-Perl software with another non-Perl software?
        * Need to discuss in more depth and look at actual code etc

    * BigCode  [ NO CHANGE 20240808 ]
        * LLM training procedures & datasets for many programming languages
        * They take all their code from GitHub w/out any curating
        * Will says this is a garbage training procedure
        * Yes Dr. Christos accepted into BigCode! :-D
            * Dr. Christos thinks the BigCode group will only approve their own requests for GPU usage
            * NEED WRITE 1 PAGE PROPOSAL AND SUBMIT ANYWAY

    * Code Llama Group (Meta?)  [ NO CHANGE 20240808 ]
        * NEED JOHN TO FIND OUT, same as BigCode above

    * Hugging Face  [ NO CHANGE 20240808 ]
        * GPUZero
            * Not for training or fine-tuning, only for running inference on pre-existing LLMs
            * Does not help us traing PerlGPT

    * Competing with Python  [ NO CHANGE 20240808 ]
        * Need ported to Perl:
            * OpenAI
            * LangChain
            * Autogen
            * Oobabooga
            * LlamaIndex
            * Ollama
            * BigCode

    * TPC Science Track Talks  [ NO CHANGE 20240808 ]
        * Structure Based Structuring of Unstructured Data
            * Dr. Adam Russell
            * Uses LLMs to produce RDF & OWL output
            * https://www.youtube.com/watch?v=dn9msFIED-8
            * NEED PAPER LINK
        * PerlGPT paper
            * Will Braswell
            * https://www.youtube.com/watch?v=Agw6E1omIvY
            * NEED PAPER LINK
        * Reasoning About the Rigor of Perl Programs
            * George Baugh
            * Wants to use AI::Categorizer
            * https://www.youtube.com/watch?v=EgpWWt1R11U
            * NEED PAPER LINK

    * TPC Normal Track Talks
        * The State of the Onion AI
            * NEED CHECK WITH JOSHUA TURCOTTE FOR MISSING VIDEO?
            * NEED VIDEO & PAPER LINKS

    * Genetic Programming
        * Adam Russell working on this right now
        * More possible now that Dr. Russell is moving to a new less-restrictive job
        * NEED MORE INFO & DISCUSSION

    * Rackspace
        * Josh R has $1K credit on Rackspace, expiring in ~60 days
        * Will asked Josh for the actual expiration date
            * NEED REPLY FROM JOSH
        * NEED JOHN & JOSH TO CHECK COST OF AI TRAINING SYSTEMS

    * 2nd Annual Perl Committee Convention, 2025
        * none

    * RoboPerl Nonprofit
        * Fundraising Committee
            * Provides funding and financial support for all RoboPerl technology committees
            * Monday 8/19/24 10:00am Central time zone, first meeting
        * Marketing Committee
            * Provides marketing, sales, and promotions for all RoboPerl technology committees
            * Monday 8/19/24 10:30am Central time zone, first meeting
        * Board of Advisors
            * Provides advice to Board of Directors on how to allocate funds received by Fundraising Committee
            * Groups providing funds get to have a seat on this board
        * Board of Directors
            * Official legal officers of RoboPerl nonprofit, responsible for all long-term decisions

* New Business
    * none

* Closing, 2:34pm
