# AI Perl Committee
# Meeting Agenda & Minutes

## August 24th, 2023
## Second Official Meeting

* Opening, 1:05pm

* Attendance & Introductions
    * Will Braswell (creator of Navi AI)
    * Drew O'Neil (AKA Andrew, uses Perl AI for chemometrics)
    * David Warner (AI sales & marketing)
    * Brett Estrade (high-speed computing)
    * Antanas Vaitkus (University in Lithuania, crystallography, wants to learn AI)
    * Somanath Wagh (Mumbai, India; wants to learn AI)
    * Leo Grapendaal (Dutchman living in Germany, interested in AI generating text for publications)
    * Glenn Holmgren (no introduction, audio problems)

* Announcements
    * Meet every 2 weeks on Thursday 2:15-3:00pm Central time zone
        * Immediately following Science Perl Committee and Perl::Types Committee meetings
    * Bylaws discussion starting 9/7/23 at next meeting

* Previous Meeting's Minutes
    * 8/10/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Brett Estrade to be accepted as read, voted unanimously

* Old Business

    * TensorFlow Phase 2, TPF grant proposal
        * Phase 2 declined by TPF Grants Committee
        * No appeals allowed, unkind feedback provided by Grants Committee, currently being contested directly w/ TPF President & board

    * PerlGPT Phase 1, TPF grant re-proposal
        * New proposal already submitted, waiting to be published

    * TPF Grants Committee population
        * Need knowledgeable volunteers to join the committee
        * Can not be officer in AI Perl Committee while also on TPF Grants Committee, would be conflict of interest

    * Monthly or Quarterly AI Perl Hackathon
        * Everyone present is interested
        * Wed Sep 6th, 7-9pm Central time, topic is Machine Vision by Will Braswell
        * Tentatively 1st Weds of each month?

    * Navi machine group purchase
        * Need 10 people to contribute $300 each so AI Perl Committee can purchase our own Navi machine
        * Need to increase level of interest & involvement before committing to group purchase

* New Business

    * TPF report
        * Will Braswell presented AI Perl Committee overview to TPF Community Reps Committee 8/18/23

    * ChatGPU Navi v1.5 demo
        * Now using Llama v2
        * Drew O'Neil, questions about chemometrics & instrumental data sources

    * Officer Elections
        * Chairman, John Napiorkowski nominated by Will Braswell, no other nominations, elected unanimously
        * 1st Vice Chairman, Will Braswell nominated by Brett Estrade, no other nominations, elected unanimously

* Closing, 2:03pm
