# AI Perl Committee

We are the official democratically-elected organization for oversight of artificial intelligence in Perl.

## Vision Statement

Our vision is a world where Perl is the dominant language in machine learning and artificial intelligence development.

## Mission Statement

Our mission is to develop and promote ethical artificial intelligence software in Perl.
