# AI Perl Committee
# Membership Roster

## Officers

* John Napiorkowski, Chairman, elected 8/24/23, re-elected 6/25/24
* Will Braswell, 1st Vice Chairman, elected 8/24/23, re-elected 6/25/24
* Dr. Adam Russell, 2nd Vice Chairman, elected 6/25/24


## Current

* Scott Adams
* Achmad Yusri Afandi
* Haider Ali
* Nikolay Arbatov
* Christos Argyropoulos, MD, PhD
* Jeb Bacon
* Kai Baker
* Myo Bandar
* Jay Battikha
* George Baugh
* Krasimir Berov (AKA Красимир Беров)
* Tyler Bird
* Wil Blake
* Will Braswell, Co-Founder
* Anne Brown
* Julian Brown
* Guido Brugnara
* Nick Busigin
* Ryan Cabaneles
* Alex Casamassima
* Egon Choroba
* Mitchko Christophe
* Smart Code
* Bar Bojan Coders
* Joshua Day
* Prashant Deore
* Roger Doss
* Brett Estrade, Co-Founder
* Ricardo Filipo
* Krzysztof Flis
* Cheok Yin "CY" Fung
* Kieren Goldfish
* Alex Gomez
* Ahmed Gouda
* Leo Grapendaal
* Henrique Gusmao
* Sangyong Gwak
* Zahirul Haq
* Dan Harris
* Robbie Hatley
* Glenn Holmgren
* Stefan Hornburg
* Nedzad Hrnjica
* Keith Hsieh
* Tracey Jacoby
* Anuj Jain
* Witold Janczar
* Bartosz Jarzyna
* Sumedha Jeewan
* Eric Jeskey
* Lewis Johanne
* Lee Johnson
* John D Jones III
* Dimitris Kechagias
* Justin Kelly
* Vlado Keselj
* Krzysztof Kielak
* Hansom Kim
* John Kirk
* Venkatesh Kumar
* Dirk Linder
* Abraham Llave (AKA إبراهيم مفتاح)
* Wang Lun
* Marco Aurelio Macae
* Mickey Macten
* Rohit Manjrekar
* Kusa Manohar
* John Martinez
* Anatol Mazur
* Daniel Mera
* Paul Millard
* Venkataramana Mokkapati
* Ali Moradi
* Daryl Morning
* Zakariyya Mughal, Co-Founder
* Shanti Munagapati
* Fernando Munguia
* Hisham Musa
* John Napiorkowski, Co-Founder
* Connie New
* Drew O'Neil
* Gerard Onerom
* Joshua C Oliva
* John P
* Sarswati Kumar Pandey
* Sibananda Pani
* Vaibhav Patil (AKA वैभव पाटील)
* James Pattie
* Jakub Pawlowski
* Rui Pereira
* Vugar Bakhshaliyev (AKA Rituda Perl)
* Minas Polychronidis
* Matthew Price
* Josh Rabinowitz
* Yulian Radev
* Brian Marquez Inca Roca
* Steve Rogerson
* Laurent Rosenfeld
* Stephane Roux
* Adam Russell, PhD
* Robert Ryley
* Siddharth Satyapriya
* Rajan Shah
* Daniel Sherer
* Prasanna Silva
* Saman Senju Simorangkir
* Douglas Spore
* Benjamin Paul Suntrup
* Manickam Thanneermalai
* Thuyein Thet
* Jovan Trujillo
* Antony Use
* Antanas Vaitkus
* Duong Vu
* Somanath Wagh
* Bo Cheng Wei
* Jeff Yoak
* Ruey-Cherng Yu
* Francisco Zarabozo
* Emanuele Zeppieri
* Mohammed Zia


## Invited

* Packy Anderson
* Tommy Butler
* Camila Chavarro
* Rick Croote
* Rob De la Cruz
* Andrass Ziska Davidsen
* Jacob Dev
* Tien Du
* Boyd Duffee
* Erki Ferenc
* Sven Gelbhaar
* Ankita Ghadge-More
* Robert Grimes
* Roy Hubbard
* Uriel Lizama
* Reinier Maliepaard (AKA Barbe Vivien)
* Rajesh Kumar Mallah
* Andrew Mehta
* Daniel Mita
* Paul Mooney
* Su Mu
* Zeeshan Muhammad
* Saiful Islam Musafir
* Dharmendra Namdeo
* Sushrut Pajai
* Emil Perhinschi
* Jason Philbin
* Mohd Rashid
* Hila Sapirstein
* Mike Schienle
* Ludovic Tolhurst-Cleaver
* Brandon Wood


## Need Invite

* Mike Johnson


## Declined

* Robert Acock [ NOT INTERESTED IN AI ]
* Ashutosh Kukreti [ CUSTOMER ONLY USING PYTHON ]
* Neil Lewitton [ NO AI FOR NOW ]
* Matthew Musgrove [ NOT USING AI ]
